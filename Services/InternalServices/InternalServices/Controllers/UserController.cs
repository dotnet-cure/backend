﻿using Common.Data_Transfers_Objects;
using LogicaNegocios.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InternalServices.Controllers
{
    public class UserController : ApiController
    {

        private ControllerUser _cu = new ControllerUser();
        // GET: api/DTOUsers
        public List<DTOUser> GetDTOUsers()
        {
            return _cu.GetAll();
        }

        [HttpPost]
        public IHttpActionResult GetUser(string login)
        {
            var user = _cu.Get(login);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
    }
}
