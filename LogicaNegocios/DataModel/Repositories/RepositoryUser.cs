﻿using Persistencia.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocios.DataModel.Repositories
{
    public class RepositoryUser 
    {
        private readonly GameQuizEntities _context;
        public RepositoryUser(GameQuizEntities context)
        {
            this._context = context;
        }

        public User Get(string login)
        {
            var UserEntity = this._context.Users.Where(a => a.login == login).FirstOrDefault();
            return UserEntity;
        }
        public IQueryable<User> GetAll()
        {
            var entityList = this._context.Users.Select(s => s);
            return entityList;
        }
        public bool Any(string login)
        {
            var exists = this._context.Users.Any(s => s.login == login);
            return exists;
        }
        public void Create(User user)
        {
            this._context.Users.Add(user);
            this._context.SaveChanges();
        }
        public void Modify(User user)
        {
            var UserEntity = this.Get(user.login);
            UserEntity = user;
            this._context.SaveChanges();
        }
        public void Delete(string login)
        {
            var entity = this.Get(login);
            this._context.Users.Remove(entity);
            this._context.SaveChanges();
        }

    }
}
