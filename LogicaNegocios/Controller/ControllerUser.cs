﻿using Common.Data_Transfers_Objects;
using Common.Mappers;
using LogicaNegocios.DataModel.Repositories;
using Persistencia.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogicaNegocios.Controller
{
    public class ControllerUser
    {
        private readonly MapperUser _mapper;
        private readonly RepositoryUser _repositoryUser;

        private readonly GameQuizEntities context = new GameQuizEntities();
        public ControllerUser()
        {
            _mapper = new MapperUser();
            _repositoryUser = new RepositoryUser(context);
        }

        public void Create(DTOUser user)
        {
            try
            {
                if (_repositoryUser.Any(user.login))
                {
                    throw new Exception("Ese nombre de usuario ya existe, intente nuevamente con otro");
                }
                _repositoryUser.Create(_mapper.MapToUser(user));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTOUser Get(string login)
        {
            return _mapper.MapToDTOUser(_repositoryUser.Get(login));
        }

        public List<DTOUser> GetAll()
        {
            List<DTOUser> listUsersDTO = new List<DTOUser>();

            var listUsers = _repositoryUser.GetAll();
            foreach (var usr in listUsers)
            {
                listUsersDTO.Add(_mapper.MapToDTOUser(usr));
            }
            return listUsersDTO;
        }

        public bool Exist(string login)
        {
            try
            {
                return _repositoryUser.Any(login);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public void Modify(DTOUser user)
        {
            try
            {
                if (_repositoryUser.Any(user.login))
                {
                    throw new Exception("Ese nombre de usuario no existe, revise el formulario");
                }
                _repositoryUser.Modify(_mapper.MapToUser(user));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete (string login)
        {
            try
            {
                if (!_repositoryUser.Any(login))
                {
                    throw new Exception("Ese nombre de usuario no existe, revise el formulario");
                }
                _repositoryUser.Delete(login);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
