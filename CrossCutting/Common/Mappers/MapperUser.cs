﻿using Common.Data_Transfers_Objects;
using Persistencia.DataBase;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Common.Mappers
{
    public class MapperUser
    {
        public DTOUser MapToDTOUser(User user)
        {
            if (user == null)
                return null;

            DTOUser userDto = new DTOUser
            {
                login = user.login,
                password = user.password,
                name = user.name,
                dateob = user.dateob
            };

            return userDto;
        }

        public User MapToUser(DTOUser userDto)
        {
            if (userDto == null)
                return null;

            User user = new User
            {
                login = userDto.login,
                password = userDto.password,
                name = userDto.name,
                dateob = userDto.dateob,
            };

            return user;
        }

    }
}
