﻿using Common.Data_Transfers_Objects;
using Persistencia.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Mappers
{
    public class MapperQuestion
    {

        public DTOQuestion MapToDTOQuestion (Question question)
        {
            if (question == null)
                return null;

            DTOQuestion dtoQuestion = new DTOQuestion
            {
                id = question.id,
                game_id = question.game_id,
                quiz = question.quiz,
                ttanswer = question.ttanswer,
                points = question.points,
                image = question.image,
                question1 = question.question1,
                answers = question.answers,
                answer_correct = question.answer_correct,
                Game = (DTOGame)question.Game
            };
            return dtoQuestion;
        }

        public Question MapToQuestion(DTOQuestion dtoQuestion)
        {
            if (dtoQuestion == null)
                return null;

            Question question = new Question
            {
                id = dtoQuestion.id,
                game_id = dtoQuestion.game_id,
                quiz = dtoQuestion.quiz,
                ttanswer = dtoQuestion.ttanswer,
                points = dtoQuestion.points,
                image = dtoQuestion.image,
                question1 = dtoQuestion.question1,
                answers = dtoQuestion.answers,
                answer_correct = dtoQuestion.answer_correct,
                Game = (Game)dtoQuestion.Game
            };
            return question;
        }
    }
}
