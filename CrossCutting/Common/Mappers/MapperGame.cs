﻿using Common.Data_Transfers_Objects;
using Persistencia.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Mappers
{
    public class MapperGame
    {
        public DTOGame MapToDTOGame(Game game)
        {
            if (game == null)
                return null;
            DTOGame dtoGame = new DTOGame
            {
                id = game.id,
                User = game.User,
                title = game.title,
                description = game.description,
                @public = game.@public,
                password = game.password,
                music = game.music,
                cover = game.cover,
                date_created = game.date_created,
                times_played = game.times_played,
                visible = game.visible,
                players_count = game.players_count,
                User1 = (DTOUser)game.User1,
                Questions = (ICollection<DTOQuestion>)game.Questions

            };

            return dtoGame;
        }

        public Game MapToGame(DTOGame dtoGame)
        {
            if (dtoGame == null)
                return null;
            Game game = new Game
            {
                id = dtoGame.id,
                User = dtoGame.User,
                title = dtoGame.title,
                description = dtoGame.description,
                @public = dtoGame.@public,
                password = dtoGame.password,
                music = dtoGame.music,
                cover = dtoGame.cover,
                date_created = dtoGame.date_created,
                times_played = dtoGame.times_played,
                visible = dtoGame.visible,
                players_count = dtoGame.players_count,
                User1 = (User)dtoGame.User1,
                Questions = (ICollection<Question>)dtoGame.Questions
            };

            return game;
        }
    }
}
