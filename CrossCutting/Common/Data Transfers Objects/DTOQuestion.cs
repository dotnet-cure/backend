﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Data_Transfers_Objects
{
    public class DTOQuestion
    {
        public int id { get; set; }
        public Nullable<int> game_id { get; set; }
        public Nullable<bool> quiz { get; set; }
        public Nullable<System.TimeSpan> ttanswer { get; set; }
        public Nullable<int> points { get; set; }
        public string image { get; set; }
        public string question1 { get; set; }
        public string answers { get; set; }
        public Nullable<int> answer_correct { get; set; }

        public virtual DTOGame Game { get; set; }
    }
}
