﻿using Persistencia.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Data_Transfers_Objects
{
    public partial class DTOUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DTOUser()
        {
            this.Games = new HashSet<DTOGame>();
        }
        [Key]
        public string login { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public Nullable<System.DateTime> dateob { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DTOGame> Games { get; set; }

        
        public static explicit operator DTOUser(User var)
        {
            return (DTOUser)var;
        }
        public static explicit operator User(DTOUser var)
        {
            return (User)var;
        }
        
    }
}
