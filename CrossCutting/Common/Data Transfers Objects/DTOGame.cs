﻿using Persistencia.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Data_Transfers_Objects
{
    public class DTOGame
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DTOGame()
        {
            this.Questions = new HashSet<DTOQuestion>();
        }
        public int id { get; set; }
        public string User { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public Nullable<bool> @public { get; set; }
        public string password { get; set; }
        public string music { get; set; }
        public string cover { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<int> times_played { get; set; }
        public Nullable<bool> visible { get; set; }
        public Nullable<int> players_count { get; set; }
        public virtual DTOUser User1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DTOQuestion> Questions { get; set; }
        
        public static explicit operator DTOGame(Game v)
        {
            return (DTOGame)v;
        }

        public static explicit operator Game(DTOGame v)
        {
            return (Game)v;
        }
        
    }

}
