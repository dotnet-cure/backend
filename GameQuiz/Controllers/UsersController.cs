﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Common.Data_Transfers_Objects;
using GameQuiz;
using LogicaNegocios.Controller;

namespace GameQuiz.Controllers
{
    public class UsersController : ApiController
    {
        private GameQuizContext db = new GameQuizContext();
        private ControllerUser _cu = new ControllerUser();
        // GET: api/DTOUsers
        public List<DTOUser> GetDTOUsers()
        {
            return _cu.GetAll();
        }

        // GET: api/DTOUsers/5
        [ResponseType(typeof(DTOUser))]
        public IHttpActionResult GetDTOUser(string id)
        {
            DTOUser dTOUser = _cu.Get(id);
            if (dTOUser == null)
            {
                return NotFound();
            }

            return Ok(dTOUser);
        }

        // PUT: api/DTOUsers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDTOUser(string id, DTOUser dTOUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dTOUser.login)
            {
                return BadRequest();
            }

            db.Entry(dTOUser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_cu.Exist(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DTOUsers
        [ResponseType(typeof(DTOUser))]
        public IHttpActionResult PostDTOUser(DTOUser dTOUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _cu.Create(dTOUser);
                return Ok(dTOUser);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            //return CreatedAtRoute("DefaultApi", new { id = dTOUser.login }, dTOUser);
        }

        // DELETE: api/DTOUsers/5
        [ResponseType(typeof(DTOUser))]
        public IHttpActionResult DeleteDTOUser(string id)
        {

            if (!_cu.Exist(id))
            {
                return NotFound();
            }

            _cu.Delete(id);

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}