﻿using Common.Data_Transfers_Objects;
using LogicaNegocios.Controller;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace GameQuiz.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
      
                ControllerUser cu = new ControllerUser();
            DTOUser usr = new DTOUser()
            {
                login = collection["login"],
                name = collection["name"],
                password = collection["password"],
                dateob = collection["dateob"].AsDateTime(),
                Games = new Collection<DTOGame>() //Ya que es la creacion, se le coloca en vacio
            };
                cu.Create(usr);
                return RedirectToAction("Index");
      
        }

        public ActionResult List()
        {
            ControllerUser cu = new ControllerUser();

            return View(cu.GetAll());
        }


    }
}
